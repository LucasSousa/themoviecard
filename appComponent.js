import React from 'react';
import { StyleSheet, Text, View, Image, FlatList, Button } from 'react-native';
import axios from "axios";
import MovieCards from './src/shared/moveCard/moveCard';

export default class App extends React.Component {
    static navigationOptions ={
        title:'Movies',
        headerStyle: {
          backgroundColor: '#4169E1',  
        },
        headerTitleStyle: {
          textAlign: 'center',
          color: '#FEFFFF',
          fontSize: 24,
          marginTop: 5,
          width: "90%",
          justifyContent: 'center',
      },
       
    };
state = {
  movies: [],
  loading: true,
  page: 1,
}

  componentDidMount() {
    let url = "https://api.themoviedb.org/3/movie/popular?api_key=53c6bcb2746b42c20afb925c1e79ce51&page=" + this.state.page;
    axios.get(url).then((response) => {
        this.setState ({
          movies: this.state.movies.concat(response.data.results) ,
          loading: false

        },
      );
    })
  } //fecha componente DidMouth

  reloadpage() {
    this.setState({
      page: this.state.page +1
    }, () => {
      this.componentDidMount()
    })
  }
  
  render() {
    
    return (
      <View style={styles.container}>
        
        {this.state.loading ? <Text style={styles.loadingConfig}>Loading...</Text>: null}
          <FlatList
            data={this.state.movies}
            renderItem={({item}) => <MovieCards movies={item} />}
            keyExtractor={(item) => item.id.toString()}
            onEndReached={() => { 
              this.reloadpage()
            }}
          />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#DCDCDC',
    padding: 16,
  },
  loadingConfig: {
    fontSize: 25, 
  }
  });
