import { createStackNavigator } from 'react-navigation';
import App from "./appComponent";

export default createStackNavigator({
  HomeScreen: App 
});